﻿using iText.IO.Image;
using iText.Kernel.Colors;
using iText.Kernel.Pdf;
using iText.Layout;
using iText.Layout.Element;
using iText.Layout.Properties;
using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace project
{
    internal class Billing
    {
        public Client client = new Client();
        public Employee employee = new Employee();
        public MedicalEquipment medicalEquipment = new MedicalEquipment();
        public Bill bill = new Bill();
        private Employee existingEmployee;
        private Client existingClient;
        private MedicalEquipment existingProduct;
        private readonly ClientManager clientManager = new ClientManager("dbConnection");
        private readonly MedicalEquipmentManager medicalEquipmentManager = new MedicalEquipmentManager("dbConnection");
        private readonly BillManager billManager = new BillManager("dbConnection");
        private readonly EmployeeManager employeeManager = new EmployeeManager("dbConnection");
        public Validations validation = new Validations();

        public void GenerateBill()
        {
            int quantity;
            char loopOption;
            var actualDate = DateTime.Now;

            List<Client> clientsList = clientManager.Select();
            List<MedicalEquipment> productsList = medicalEquipmentManager.Select();
            List<Employee> employeeList = employeeManager.Select();
            List<Bill> billsList = billManager.Select();
            List<Bill> generatedBill = new List<Bill>();

            int lastInvoice = (billsList.Count == 0 ? 1 : billsList.Last().invoice + 1);

            Console.WriteLine("------------------------------");
            Console.WriteLine("|           Billing          |");
            Console.WriteLine("------------------------------");

            do
            {
                employee.id = validation.ValidateIntegerPositiveValue("\nSeller ID: ");

                existingEmployee = employeeList.Find(e => e.id == employee.id);

                if (existingEmployee == null)
                    Console.WriteLine("\nEmployee wasn't found in database.");
            }
            while (existingEmployee == null);

            do
            {
                client.id = validation.ValidateIntegerPositiveValue("\nClient ID: ");

                existingClient = clientsList.Find(m => m.id == client.id);

                if (existingClient == null)
                    Console.WriteLine("\nClient wasn't found in database.");
            }
            while (existingClient == null);

            do
            {
                do
                {
                    medicalEquipment.id = validation.ValidateIntegerPositiveValue("\nProduct ID: ");

                    existingProduct = productsList.Find(m => m.id == medicalEquipment.id);

                    if (existingProduct == null)
                        Console.WriteLine("\nProduct wasn't found in database.");
                }
                while (existingProduct == null);

                do
                {
                    quantity = validation.ValidateIntegerPositiveValue("\nQuantity: ");

                    if (existingProduct.quantity < quantity)
                        Console.WriteLine("\nProduct doesn't have enough existences for selling.");
                }
                while (existingProduct.quantity < quantity || existingProduct.quantity == 0);

                double subtotal = existingProduct.sellingPrice * quantity;
                double tax = subtotal * 0.16;
                double total = subtotal + tax;
                int discountQuantity = existingProduct.quantity - quantity;
                double commission = total * 0.02;
                var getActualEmployee = employeeManager.Select();
                var actualEmployee = getActualEmployee.Find(e => e.id == employee.id);

                MedicalEquipment updateExistences = new MedicalEquipment
                {
                    id = existingProduct.id,
                    name = existingProduct.name,
                    quantity = discountQuantity,
                    trademark = existingProduct.trademark,
                    model = existingProduct.model,
                    lot = existingProduct.lot,
                    buyingPrice = existingProduct.buyingPrice,
                    sellingPrice = existingProduct.sellingPrice
                };

                Employee actualEmployeeCommission = new Employee
                {
                    id = actualEmployee.id,
                    name = actualEmployee.name,
                    rfc = actualEmployee.rfc,
                    role = actualEmployee.role,
                    salary = actualEmployee.salary,
                    commission = actualEmployee.commission + commission
                };

                bill.invoice = lastInvoice;
                bill.date = actualDate;
                bill.clientId = existingClient.id;
                bill.clientName = existingClient.name;
                bill.clientRfc = existingClient.rfc;
                bill.productId = existingProduct.id;
                bill.productName = existingProduct.name;
                bill.quantity = quantity;
                bill.productTrademark = existingProduct.trademark;
                bill.productModel = existingProduct.model;
                bill.unitaryPrice = existingProduct.sellingPrice;
                bill.subtotal = subtotal;
                bill.taxes = tax;
                bill.totalPrice = total;

                employeeManager.UpdateCommissions(actualEmployeeCommission);
                medicalEquipmentManager.UpdateExistences(updateExistences);
                billManager.Insert(bill);

                generatedBill.Add(new Bill
                {
                    date = actualDate,
                    clientName = existingClient.name,
                    clientRfc = existingClient.rfc,
                    productName = bill.productName,
                    quantity = quantity,
                    productTrademark = bill.productTrademark,
                    productModel = bill.productModel,
                    unitaryPrice = existingProduct.sellingPrice,
                    subtotal = subtotal,
                    taxes = tax,
                    totalPrice = total
                });

                Console.WriteLine("\nAdd more products to bill? (y / n):");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            GeneratePdf(lastInvoice.ToString(), generatedBill);
        }

        public void GeneratePdf(string invoice, List<Bill> generatedBill)
        {
            var billDate = generatedBill.FirstOrDefault().date;
            var billClientName = generatedBill.FirstOrDefault().clientName;
            var billClientRfc = generatedBill.FirstOrDefault().clientRfc;
            var fiscalInvoice = randomInvoice();
            Color customColor = new DeviceRgb(114, 167, 232);
            Image qrCode = new Image(ImageDataFactory.Create(@"C:\Users\Fideo\Downloads\default_qrcode.png"))
                .SetTextAlignment(TextAlignment.LEFT);

            PdfWriter writer = new PdfWriter($@"C:\Users\Fideo\Downloads\F000000{invoice}.pdf");
            PdfDocument pdf = new PdfDocument(writer);
            Document document = new Document(pdf);
            Paragraph newLine = new Paragraph("\n");

            Paragraph name = new Paragraph("Ingenieria Biomedica de Occidente S.A. De C.V.")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontColor(customColor)
                .SetFontSize(18);
            Paragraph rfc = new Paragraph("IBO216389H")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(16);
            Paragraph direction = new Paragraph("Calzada de los paraisos #6825")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(16);
            Paragraph tel = new Paragraph("(33) 3615-4589")
                .SetTextAlignment(TextAlignment.CENTER)
                .SetFontSize(16);
            document.Add(name);
            document.Add(rfc);
            document.Add(direction);
            document.Add(tel);

            Table clientInfo = new Table(4, false);
            Cell invoiceCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Invoice"));
            Cell dateCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Bill Date"));
            Cell clientNameCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Client Name"));
            Cell rfcCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Client Rfc"));

            clientInfo.AddCell(invoiceCell);
            clientInfo.AddCell(dateCell);
            clientInfo.AddCell(clientNameCell);
            clientInfo.AddCell(rfcCell);

            Cell invoiceVariable = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph($"F000000{invoice}"));
            Cell dateVariable = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph(billDate.ToString()));
            Cell clientNameVariable = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph(billClientName));
            Cell rfcVariable = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph(billClientRfc));

            clientInfo.AddCell(invoiceVariable);
            clientInfo.AddCell(dateVariable);
            clientInfo.AddCell(clientNameVariable);
            clientInfo.AddCell(rfcVariable);


            Table billInfo = new Table(8, false);
            Cell productNameCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Product Name"));
            Cell quantityCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Quantity"));
            Cell trademarkCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Trademark"));
            Cell modelCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Model"));
            Cell priceCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Unitary Price"));
            Cell subtotalCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Subtotal"));
            Cell taxesCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Taxes"));
            Cell totalCell = new Cell(1, 1)
               .SetBackgroundColor(customColor)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Total"));

            billInfo.AddCell(productNameCell);
            billInfo.AddCell(quantityCell);
            billInfo.AddCell(trademarkCell);
            billInfo.AddCell(modelCell);
            billInfo.AddCell(priceCell);
            billInfo.AddCell(subtotalCell);
            billInfo.AddCell(taxesCell);
            billInfo.AddCell(totalCell);


            foreach (var product in generatedBill)
            {
                Cell productNameVariable = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph(product.productName));
                Cell quantityVariable = new Cell(1, 1)
                   .SetTextAlignment(TextAlignment.LEFT)
                   .Add(new Paragraph(product.quantity.ToString()));
                Cell trademarkVariable = new Cell(1, 1)
                   .SetTextAlignment(TextAlignment.LEFT)
                   .Add(new Paragraph(product.productTrademark));
                Cell modelVariable = new Cell(1, 1)
                   .SetTextAlignment(TextAlignment.LEFT)
                   .Add(new Paragraph(product.productModel));
                Cell priceVariable = new Cell(1, 1)
                   .SetTextAlignment(TextAlignment.LEFT)
                   .Add(new Paragraph(product.unitaryPrice.ToString()));
                Cell subtotalVariable = new Cell(1, 1)
                   .SetTextAlignment(TextAlignment.LEFT)
                   .Add(new Paragraph(product.subtotal.ToString()));
                Cell taxesVariable = new Cell(1, 1)
                   .SetTextAlignment(TextAlignment.LEFT)
                   .Add(new Paragraph(product.taxes.ToString()));
                Cell totalVariable = new Cell(1, 1)
                   .SetTextAlignment(TextAlignment.LEFT)
                   .Add(new Paragraph(product.totalPrice.ToString()));

                billInfo.AddCell(productNameVariable);
                billInfo.AddCell(quantityVariable);
                billInfo.AddCell(trademarkVariable);
                billInfo.AddCell(modelVariable);
                billInfo.AddCell(priceVariable);
                billInfo.AddCell(subtotalVariable);
                billInfo.AddCell(taxesVariable);
                billInfo.AddCell(totalVariable);
            }

            Table fiscalInvoiceTable = new Table(1, false);
            Cell fiscalInvoiceCell = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph("Fiscal Invoice"));

            fiscalInvoiceTable.AddCell(fiscalInvoiceCell);

            Cell fiscalInvoiceVariable = new Cell(1, 1)
               .SetTextAlignment(TextAlignment.LEFT)
               .Add(new Paragraph(fiscalInvoice));

            fiscalInvoiceTable.AddCell(fiscalInvoiceVariable);

            document.Add(newLine);
            document.Add(clientInfo);
            document.Add(newLine);
            document.Add(billInfo);
            document.Add(newLine);
            document.Add(fiscalInvoiceTable);
            document.Add(newLine);
            document.Add(qrCode);
            document.Close();
        }

        public string randomInvoice()
        {
            Random random = new Random();

            string alphanumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            return new string(Enumerable.Repeat(alphanumeric, 35).Select(a => a[random.Next(a.Length)]).ToArray());
        }
    }
}
