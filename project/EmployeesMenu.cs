﻿using library;
using project.Operations;
using System;

namespace project
{
    internal class EmployeesMenu
    {
        public AdministratorMenu administratorMenu = new AdministratorMenu();
        public EmployeeExecution execution = new EmployeeExecution();
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;

            do
            {
                Console.WriteLine("1. New employee.");
                Console.WriteLine("2. Delete employee.");
                Console.WriteLine("3. Modify employee.");
                Console.WriteLine("4. Employees info.");
                Console.WriteLine("5. Administrator menu.");
                option = validations.ValidateIntegerPositiveValue("\nOption: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        execution.NewEmployee();
                        administratorMenu.Menu();
                    break;

                    case 2:
                        Console.Clear();
                        execution.DeleteEmployee();
                        administratorMenu.Menu();
                    break;

                    case 3:
                        Console.Clear();
                        execution.UpdateEmployee();
                        administratorMenu.Menu();
                    break;

                    case 4:
                        Console.Clear();
                        execution.ListAllEmployees();
                        administratorMenu.Menu();
                    break;

                    case 5:
                        Console.Clear();
                        administratorMenu.Menu();
                        break;
                }
            }
            while (option < 1 || option > 5);
        }
    }
}