﻿using library;
using project.Operations;
using System;

namespace project
{
    internal class MedicalEquipmentMenu
    {
        public AdministratorMenu administratorMenu = new AdministratorMenu();
        public ProductExecution execution = new ProductExecution();
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;

            do
            {
                Console.WriteLine("1. New product.");
                Console.WriteLine("2. Delete product.");
                Console.WriteLine("3. Modify product.");
                Console.WriteLine("4. Inventory info.");
                Console.WriteLine("5. Administrator menu.");
                option = validations.ValidateIntegerPositiveValue("\nOption: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        execution.NewProduct();
                        administratorMenu.Menu();
                    break;

                    case 2:
                        Console.Clear();
                        execution.DeleteProduct();
                        administratorMenu.Menu();
                    break;

                    case 3:
                        Console.Clear();
                        execution.UpdateProduct();
                        administratorMenu.Menu();
                    break;

                    case 4:
                        Console.Clear();
                        execution.ListAllProducts();
                        administratorMenu.Menu();
                    break;

                    case 5:
                        Console.Clear();
                        administratorMenu.Menu();
                    break;
                }
            }
            while (option < 1 || option > 5);
        }
    }
}