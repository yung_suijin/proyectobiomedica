﻿using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace project.Operations
{
    internal class ProviderExecution
    {
        private Provider provider = new Provider();
        private Provider existingProvider;
        private readonly ProviderManager providerManager = new ProviderManager("dbConnection");
        public Validations validation = new Validations();
        public Additions additions = new Additions();
        public ModelValidation<Provider> validateProvider = new ModelValidation<Provider>();
        public List<ValidationResult> errors = new List<ValidationResult>();
        public char loopOption;

        public void NewProvider()
        {
            do
            {
                do
                {
                    List<Provider> providerList = providerManager.Select();

                    do
                    {
                        provider.id = validation.ValidateIntegerPositiveValue("Provider ID: ");

                        existingProvider = providerList.Find(m => m.id == provider.id);

                        if (existingProvider != null)
                            Console.WriteLine("\nProvider is already in database and cannot be duplicated.");
                    }
                    while (existingProvider != null || provider.id < 0);

                    Console.Write("\nProvider Name: ");
                    provider.name = Console.ReadLine();
                    Console.Write("\nProvider RFC: ");
                    provider.rfc = Console.ReadLine();
                    Console.Write("\nProvider Direction: ");
                    provider.direction = Console.ReadLine();

                    errors = validateProvider.ValidateObject(provider);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (providerManager.Insert(provider))
                    Console.WriteLine("\nProvider was inserted into database succesfully.");
                else
                    Console.WriteLine("\nProvider couldn't be inserted into database.");

                Console.Write("Add another provider? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void DeleteProvider()
        {
            do
            {
                List<Provider> providerList = providerManager.Select();

                do
                {
                    provider.id = validation.ValidateIntegerPositiveValue("Provider ID: ");

                    existingProvider = providerList.Find(m => m.id == provider.id);

                    if (existingProvider == null)
                        Console.WriteLine("\nProvider wasn't found in database and cannot be deleted.");
                }
                while (existingProvider == null);

                if (providerManager.Delete(provider))
                    Console.WriteLine("\nProvider was deleted from database succesfully.");
                else
                    Console.WriteLine("\nProvider couldn't be deleted from database.");

                Console.Write("Delete another provider? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());

            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void UpdateProvider()
        {
            do
            {
                do
                {
                    List<Provider> providerList = providerManager.Select();

                    do
                    {
                        provider.id = validation.ValidateIntegerPositiveValue("Provider ID: ");

                        existingProvider = providerList.Find(m => m.id == provider.id);

                        if (existingProvider == null)
                            Console.WriteLine("\nProvider wasn't found in database and cannot be modified.");
                    }
                    while (existingProvider == null);

                    Console.Write("\nProvider Name: ");
                    provider.name = Console.ReadLine();
                    Console.Write("\nProvider RFC: ");
                    provider.rfc = Console.ReadLine();
                    Console.Write("\nProvider Direction: ");
                    provider.direction = Console.ReadLine();

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (providerManager.Update(provider))
                    Console.WriteLine("\nProvider was updated in the database succesfully.");
                else
                    Console.WriteLine("\nProvider couldn't be updated in the database.");

                Console.Write("Update another provider? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void ListAllProviders()
        {
            List<Provider> providerList = providerManager.Select();

            string id = "ID", name = "Name", rfc = "RFC", direction = "Direction";

            Console.WriteLine($"{id.PadRight(5)}{name.PadRight(40)}{rfc.PadRight(15)}{direction.PadRight(40)}\n");

            foreach (var providers in providerList)
            {
                Console.WriteLine(providers.id.ToString().PadRight(5) +
                    providers.name.PadRight(40) +
                    providers.rfc.PadRight(15) +
                    providers.direction.PadRight(40));
            }

            additions.ClearScreenAfterKey();
        }
    }
}
