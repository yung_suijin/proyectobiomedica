﻿using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace project.Operations
{
    internal class UserExecution
    {
        private User user = new User();
        private User existingUser;
        private readonly UserManager userManager = new UserManager("dbConnection");
        public Validations validation = new Validations();
        public Additions additions = new Additions();
        public ModelValidation<User> validateUser = new ModelValidation<User>();
        public List<ValidationResult> errors = new List<ValidationResult>();
        private readonly List<int> levels = new List<int>()
        {
            1, 2, 3
        };
        public char loopOption;

        public void NewUser()
        {
            do
            {
                do
                {
                    List<User> userList = userManager.Select();

                    do
                    {
                        user.id = validation.ValidateIntegerPositiveValue("User ID: ");

                        existingUser = userList.Find(m => m.id == user.id);

                        if (existingUser != null)
                            Console.WriteLine("\nUser is already in database and cannot be duplicated.");
                    }
                    while (existingUser != null || user.id < 0);

                    Console.Write("\nUser Password: ");
                    user.password = Console.ReadLine();

                    do
                    {
                        user.level = validation.ValidateIntegerPositiveValue("\nUser Level: ");
                    }
                    while (!levels.Contains(user.level) || user.level < 0);

                    errors = validateUser.ValidateObject(user);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (userManager.Insert(user))
                    Console.WriteLine("\nUser was inserted into database succesfully.");
                else
                    Console.WriteLine("\nUser couldn't be inserted into database.");

                Console.Write("Add another user? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void DeleteUser()
        {
            do
            {
                List<User> userList = userManager.Select();

                do
                {
                    user.id = validation.ValidateIntegerPositiveValue("User ID: ");

                    existingUser = userList.Find(m => m.id == user.id);

                    if (existingUser == null)
                        Console.WriteLine("\nUser wasn't found in database and cannot be deleted.");
                }
                while (existingUser == null);

                if (userManager.Delete(user))
                    Console.WriteLine("\nUser was deleted from database succesfully.");
                else
                    Console.WriteLine("\nUser couldn't be deleted from database.");

                Console.Write("Delete another user? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());

            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void UpdateUser()
        {
            do
            {
                do
                {
                    List<User> userList = userManager.Select();

                    do
                    {
                        user.id = validation.ValidateIntegerPositiveValue("User ID: ");

                        existingUser = userList.Find(m => m.id == user.id);

                        if (existingUser == null)
                            Console.WriteLine("\nUser wasn't found in database and cannot be modified.");
                    }
                    while (existingUser == null);

                    Console.Write("\nUser Password: ");
                    user.password = Console.ReadLine();

                    do
                    {
                        user.level = validation.ValidateIntegerPositiveValue("\nUser Level: ");
                    }
                    while (!levels.Contains(user.level) || user.level < 0);

                    errors = validateUser.ValidateObject(user);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (userManager.Update(user))
                    Console.WriteLine("\nUser was updated in the database succesfully.");
                else
                    Console.WriteLine("\nUser couldn't be updated in the database.");

                Console.Write("Update another user? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void ListAllUsers()
        {
            List<User> userList = userManager.Select();

            string id = "ID", level = "Level";

            Console.WriteLine($"{id.PadRight(5)}{level.PadRight(7)}\n");

            foreach (var users in userList)
            {
                Console.WriteLine(users.id.ToString().PadRight(5) +
                    users.level.ToString().PadRight(7));
            }

            additions.ClearScreenAfterKey();
        }
    }
}