﻿using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace project.Operations
{
    internal class ProductExecution
    {
        private MedicalEquipment medicalEquipment = new MedicalEquipment();
        private MedicalEquipment existingProduct;
        private readonly MedicalEquipmentManager medicalEquipmentManager = new MedicalEquipmentManager("dbConnection");
        public Validations validation = new Validations();
        public Additions additions = new Additions();
        public ModelValidation<MedicalEquipment> validateProduct = new ModelValidation<MedicalEquipment>();
        public List<ValidationResult> errors = new List<ValidationResult>();
        public char loopOption;

        public void NewProduct()
        {
            do
            {
                do
                {
                    List<MedicalEquipment> medicalEquipmentList = medicalEquipmentManager.Select();

                    do
                    {
                        medicalEquipment.id = validation.ValidateIntegerPositiveValue("Product ID: ");

                        existingProduct = medicalEquipmentList.Find(m => m.id == medicalEquipment.id);

                        if (existingProduct != null)
                            Console.WriteLine("\nProduct is already in database and cannot be duplicated.");
                    }
                    while (existingProduct != null || medicalEquipment.id < 0);

                    Console.Write("\nProduct Name: ");
                    medicalEquipment.name = Console.ReadLine();
                    Console.Write("\nProduct Trademark: ");
                    medicalEquipment.trademark = Console.ReadLine();
                    Console.Write("\nProduct Model: ");
                    medicalEquipment.model = Console.ReadLine();
                    Console.Write("\nProduct Lot: ");
                    medicalEquipment.lot = Console.ReadLine();

                    do
                    {
                        medicalEquipment.buyingPrice = validation.ValidateDoublePositiveValue("\nProduct Buying Price: ");
                        medicalEquipment.sellingPrice = validation.ValidateDoublePositiveValue("\nProduct Selling Price: ");
                    }
                    while (medicalEquipment.buyingPrice < 0 || medicalEquipment.sellingPrice < 0);

                    errors = validateProduct.ValidateObject(medicalEquipment);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (medicalEquipmentManager.Insert(medicalEquipment))
                    Console.WriteLine("\nProduct was inserted into database succesfully.");
                else
                    Console.WriteLine("\nProduct couldn't be inserted into database.");

                Console.Write("Add another product? (s/n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 's');

            additions.ClearScreenAfterKey();
        }

        public void DeleteProduct()
        {
            do
            {
                List<MedicalEquipment> medicalEquipmentList = medicalEquipmentManager.Select();
            
                do
                {
                    medicalEquipment.id = validation.ValidateIntegerPositiveValue("Product ID: ");

                    existingProduct = medicalEquipmentList.Find(m => m.id == medicalEquipment.id);

                    if (existingProduct == null)
                        Console.WriteLine("\nProduct wasn't found in database and cannot be deleted.");
                }
                while (existingProduct == null);

                if (medicalEquipmentManager.Delete(medicalEquipment))
                    Console.WriteLine("\nProduct was deleted from database succesfully.");
                else
                    Console.WriteLine("\nProduct couldn't be deleted from database.");

                Console.Write("Delete another product? (s/n): ");
                loopOption = Char.Parse(Console.ReadLine());

            }
            while (loopOption == 's');

            additions.ClearScreenAfterKey();
        }

        public void UpdateProduct()
        {
            do
            {
                do
                {
                    List<MedicalEquipment> medicalEquipmentList = medicalEquipmentManager.Select();

                    do
                    {
                        medicalEquipment.id = validation.ValidateIntegerPositiveValue("Product ID: ");

                        existingProduct = medicalEquipmentList.Find(m => m.id == medicalEquipment.id);

                        if (existingProduct == null)
                            Console.WriteLine("\nProduct wasn't found in database and cannot be modified.");
                    }
                    while (existingProduct == null);

                    Console.Write("\nProduct Name: ");
                    medicalEquipment.name = Console.ReadLine();
                    Console.Write("\nProduct Trademark: ");
                    medicalEquipment.trademark = Console.ReadLine();
                    Console.Write("\nProduct Model: ");
                    medicalEquipment.model = Console.ReadLine();
                    Console.Write("\nProduct Lot: ");
                    medicalEquipment.lot = Console.ReadLine();

                    do
                    {
                        medicalEquipment.buyingPrice = validation.ValidateDoublePositiveValue("\nProduct Buying Price:");
                        medicalEquipment.sellingPrice = validation.ValidateDoublePositiveValue("\nProduct Selling Price: ");
                    }
                    while (medicalEquipment.buyingPrice < 0 || medicalEquipment.sellingPrice < 0);

                    errors = validateProduct.ValidateObject(medicalEquipment);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (medicalEquipmentManager.Update(medicalEquipment))
                    Console.WriteLine("\nProduct was updated in the database succesfully.");
                else
                    Console.WriteLine("\nProduct couldn't be updated in the database.");

                Console.Write("Update another product? (s/n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 's');

            additions.ClearScreenAfterKey();
        }

        public void ListAllProducts()
        {
            List<MedicalEquipment> medicalEquipmentList = medicalEquipmentManager.Select();

            string id = "ID", name = "Name", quantity = "Quantity", trademark = "Trademark",
                        model = "Model", lot = "Lot", priceBuy = "Buying price", priceSell = "Selling price";

            Console.WriteLine($"{id.PadRight(5)}{name.PadRight(40)}{quantity.PadRight(10)}{trademark.PadRight(15)}" +
                $"{model.PadRight(15)}{lot.PadRight(15)}{priceBuy.PadRight(15)}{priceSell.PadRight(15)}\n");

            foreach (var product in medicalEquipmentList)
            {
                Console.WriteLine(product.id.ToString().PadRight(5) +
                    product.name.PadRight(40) +
                    product.quantity.ToString().PadRight(10) +
                    product.trademark.PadRight(15) +
                    product.model.PadRight(15) +
                    product.lot.PadRight(15) +
                    product.buyingPrice.ToString().PadRight(15) +
                    product.sellingPrice.ToString().PadRight(15));
            }

            additions.ClearScreenAfterKey();
        }
    }
}