﻿using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace project.Operations
{
    internal class EmployeeExecution
    {
        private Employee employee = new Employee();
        private Employee existingEmployee;
        private EmployeeManager employeeManager = new EmployeeManager("dbConnection");
        public Validations validation = new Validations();
        public Additions additions = new Additions();
        public ModelValidation<Employee> validateEmployee = new ModelValidation<Employee>();
        public List<ValidationResult> errors = new List<ValidationResult>();
        public char loopOption;

        public void NewEmployee()
        {
            do
            {
                do
                {
                    List<Employee> employeeList = employeeManager.Select();

                    do
                    {
                        employee.id = validation.ValidateIntegerPositiveValue("Employee ID: ");

                        existingEmployee = employeeList.Find(m => m.id == employee.id);

                        if (existingEmployee != null)
                            Console.WriteLine("\nEmployee is already in database and cannot be duplicated.");
                    }
                    while (existingEmployee != null || employee.id < 0);

                    Console.Write("\nEmployee Name: ");
                    employee.name = Console.ReadLine();
                    Console.Write("\nEmployee RFC: ");
                    employee.rfc = Console.ReadLine();
                    Console.Write("\nEmployee Role: ");
                    employee.role = Console.ReadLine();

                    do
                    {
                        employee.salary = validation.ValidateDoublePositiveValue("\nEmployee Salary: ");
                    }
                    while (employee.salary < 0);

                    errors = validateEmployee.ValidateObject(employee);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (employeeManager.Insert(employee))
                    Console.WriteLine("\nEmployee was inserted into database succesfully.");
                else
                    Console.WriteLine("\nEmployee couldn't be inserted into database.");

                Console.Write("Add another employee? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void DeleteEmployee()
        {
            do
            {
                List<Employee> employeeList = employeeManager.Select();

                do
                {
                    employee.id = validation.ValidateIntegerPositiveValue("Employee ID: ");

                    existingEmployee = employeeList.Find(m => m.id == employee.id);

                    if (existingEmployee == null)
                        Console.WriteLine("\nEmployee wasn't found in database and cannot be deleted.");
                }
                while (existingEmployee == null);

                if (employeeManager.Delete(employee))
                    Console.WriteLine("\nEmployee was deleted from database succesfully.");
                else
                    Console.WriteLine("\nEmployee couldn't be deleted from database.");

                Console.Write("Delete another employee? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());

            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void UpdateEmployee()
        {
            do
            {
                do
                {
                    List<Employee> employeeList = employeeManager.Select();

                    do
                    {
                        employee.id = validation.ValidateIntegerPositiveValue("Employee ID: ");

                        existingEmployee = employeeList.Find(m => m.id == employee.id);

                        if (existingEmployee == null)
                            Console.WriteLine("\nEmployee wasn't found in database and cannot be modified.");
                    }
                    while (existingEmployee == null);

                    Console.Write("\nEmployee Name: ");
                    employee.name = Console.ReadLine();
                    Console.Write("\nEmployee RFC: ");
                    employee.rfc = Console.ReadLine();
                    Console.Write("\nEmployee Role: ");
                    employee.role = Console.ReadLine();

                    do
                    {
                        employee.salary = validation.ValidateDoublePositiveValue("\nEmployee Salary: ");
                    }
                    while (employee.salary < 0);

                    errors = validateEmployee.ValidateObject(employee);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (employeeManager.Update(employee))
                    Console.WriteLine("\nEmployee was updated in the database succesfully.");
                else
                    Console.WriteLine("\nEmployee couldn't be updated in the database.");

                Console.Write("Update another employee? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void ListAllEmployees()
        {
            List<Employee> employeeList = employeeManager.Select();

            string id = "ID", name = "Name", rfc = "RFC", role = "Role", salary = "Salary", commissions = "Commissions";

            Console.WriteLine($"{id.PadRight(5)}{name.PadRight(40)}{rfc.PadRight(15)}{role.PadRight(20)}{salary.PadRight(10)}{commissions.PadRight(10)}\n");

            foreach (var employees in employeeList)
            {
                Console.WriteLine(employees.id.ToString().PadRight(5) +
                    employees.name.PadRight(40) +
                    employees.rfc.PadRight(15) +
                    employees.role.PadRight(20) +
                    employees.salary.ToString().PadRight(10) +
                    employees.commission.ToString().PadRight(10));
            }

            additions.ClearScreenAfterKey();
        }
    }
}
