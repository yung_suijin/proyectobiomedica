﻿using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace project.Operations
{
    internal class PurchaseExecution
    {
        private Provider provider = new Provider();
        private Provider existingProvider;
        private readonly ProviderManager providerManager = new ProviderManager("dbConnection");
        private MedicalEquipment medicalEquipment = new MedicalEquipment();
        private MedicalEquipment existingProduct;
        private readonly MedicalEquipmentManager medicalEquipmentManager = new MedicalEquipmentManager("dbConnection");
        private Purchase purchase = new Purchase();
        private readonly PurchaseManager purchaseManager = new PurchaseManager("dbConnection");
        public Validations validation = new Validations();
        public Additions additions = new Additions();
        public string id = "ID", name = "Name", buyDate = "Date", model = "Model", quantity = "Quantity", unitaryPrice = "Unitary Price",
            total = "Total", taxes = "Taxes", commission = "Commission", productName = "Product", subtotal = "Subtotal";

        public void BuyProduct()
        {
            int quantity;
            char loopOption;
            DateTime actualDate = DateTime.Now;
            List<Provider> providerList = providerManager.Select();
            List<MedicalEquipment> productList = medicalEquipmentManager.Select();
            List<Purchase> purchaseList = purchaseManager.Select();
            int lastPurchaseId = (purchaseList.Count == 0 ? 1 : purchaseList.Last().purchaseId + 1);

            do
            {
                provider.id = validation.ValidateIntegerPositiveValue("Provider ID: ");

                existingProvider = providerList.Find(m => m.id == provider.id);

                if (existingProvider == null)
                    Console.WriteLine("\nProvider wasn't found in database.");
            }
            while (existingProvider == null);

            do
            {

                do
                {
                    medicalEquipment.id = validation.ValidateIntegerPositiveValue("\nProduct ID: ");

                    existingProduct = productList.Find(m => m.id == medicalEquipment.id);

                    if (existingProduct == null)
                        Console.WriteLine("\nProduct wasn't found in database.");
                }
                while (existingProduct == null);

                quantity = validation.ValidateIntegerPositiveValue("\nQuantity: ");

                int addQuantity = existingProduct.quantity + quantity;
                double subtotal = existingProduct.buyingPrice * quantity;
                double tax = subtotal * 0.16;
                double total = subtotal + tax;

                MedicalEquipment updateExistences = new MedicalEquipment
                {
                    id = existingProduct.id,
                    name = existingProduct.name,
                    quantity = addQuantity,
                    trademark = existingProduct.trademark,
                    model = existingProduct.model,
                    lot = existingProduct.lot,
                    buyingPrice = existingProduct.buyingPrice,
                    sellingPrice = existingProduct.sellingPrice
                };

                purchase.purchaseId = lastPurchaseId;
                purchase.date = actualDate;
                purchase.providerId = existingProvider.id;
                purchase.providerName = existingProvider.name;
                purchase.providerRfc = existingProvider.rfc;
                purchase.productId = existingProduct.id;
                purchase.productName = existingProduct.name;
                purchase.model = existingProduct.model;
                purchase.quantity = quantity;
                purchase.buyingPrice = existingProduct.buyingPrice;
                purchase.subtotal = subtotal;
                purchase.taxes = tax;
                purchase.totalPrice = total;

                medicalEquipmentManager.UpdateExistences(updateExistences);
                purchaseManager.Insert(purchase);

                Console.Write("Purchase another product? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void PurchaseReportByProduct()
        {
            List<MedicalEquipment> medicalEquipmentList = medicalEquipmentManager.Select();
            List<Purchase> purchaseList = purchaseManager.Select();

            do
            {
                medicalEquipment.id = validation.ValidateIntegerPositiveValue("Product ID: ");

                existingProduct = medicalEquipmentList.Find(m => m.id == medicalEquipment.id);

                if (existingProduct == null)
                    Console.WriteLine("\nProduct wasn't found in database.");
            }
            while (existingProduct == null);

            var productIdList = purchaseList.Where(p => p.productId == medicalEquipment.id);

            Console.WriteLine($"\n{name.PadRight(40)}{buyDate.PadRight(15)}{model.PadRight(35)}{quantity.PadRight(13)}{unitaryPrice.PadRight(15)}" +
                $"{subtotal.PadRight(12)}{taxes.PadRight(10)}{total.PadRight(10)}\n");

            foreach (var info in productIdList)
            {
                Console.WriteLine(info.productName.PadRight(40) +
                    info.date.ToString(@"dd\/MM\/yyyy").PadRight(15) +
                    info.model.PadRight(35) +
                    info.quantity.ToString().PadRight(13) +
                    info.buyingPrice.ToString().PadRight(15) +
                    info.subtotal.ToString().PadRight(12) +
                    info.taxes.ToString().PadRight(10) +
                    info.totalPrice.ToString().PadRight(10));
            }

            additions.ClearScreenAfterKey();
        }

        public void PurchaseReportByProvider()
        {
            List<Provider> providerList = providerManager.Select();
            List<Purchase> purchaseList = purchaseManager.Select();

            do
            {
                provider.id = validation.ValidateIntegerPositiveValue("Provider ID: ");

                existingProvider = providerList.Find(m => m.id == provider.id);

                if (existingProvider == null)
                    Console.WriteLine("\nProvider wasn't found in database.");
            }
            while (existingProvider == null);

            var productIdList = purchaseList.Where(p => p.providerId == provider.id);

            Console.WriteLine($"\n{name.PadRight(40)}{buyDate.PadRight(15)}{model.PadRight(35)}{quantity.PadRight(13)}{unitaryPrice.PadRight(15)}" +
                $"{subtotal.PadRight(12)}{taxes.PadRight(10)}{total.PadRight(10)}\n");

            foreach (var info in productIdList)
            {
                Console.WriteLine(info.productName.PadRight(40) +
                    info.date.ToString(@"dd\/MM\/yyyy").PadRight(15) +
                    info.model.PadRight(35) +
                    info.quantity.ToString().PadRight(13) +
                    info.buyingPrice.ToString().PadRight(15) +
                    info.subtotal.ToString().PadRight(12) +
                    info.taxes.ToString().PadRight(10) +
                    info.totalPrice.ToString().PadRight(10));
            }

            additions.ClearScreenAfterKey();
        }
    }
}
