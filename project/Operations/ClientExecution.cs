﻿using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace project.Operations
{
    internal class ClientExecution
    {
        private Client client = new Client();
        private Client existingClient;
        private readonly ClientManager clientManager = new ClientManager("dbConnection");
        public Validations validation = new Validations();
        public Additions additions = new Additions();
        public ModelValidation<Client> validateClient = new ModelValidation<Client>();
        public List<ValidationResult> errors = new List<ValidationResult>();
        public char loopOption;

        public void NewClient()
        {
            do
            {
                do
                {
                    List<Client> clientList = clientManager.Select();

                    do
                    {
                        client.id = validation.ValidateIntegerPositiveValue("Client ID: ");

                        existingClient = clientList.Find(m => m.id == client.id);

                        if (existingClient != null)
                            Console.WriteLine("\nClient is already in database and cannot be duplicated.");
                    }
                    while (existingClient != null || client.id < 0);

                    Console.Write("\nClient Name: ");
                    client.name = Console.ReadLine();
                    Console.Write("\nClient RFC: ");
                    client.rfc = Console.ReadLine();
                    Console.Write("\nClient Direction: ");
                    client.direction = Console.ReadLine();

                    errors = validateClient.ValidateObject(client);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (clientManager.Insert(client))
                    Console.WriteLine("\nClient was inserted into database succesfully.");
                else
                    Console.WriteLine("\nClient couldn't be inserted into database.");

                Console.Write("Add another client? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void DeleteClient()
        {
            do
            {
                List<Client> clientList = clientManager.Select();

                do
                {
                    client.id = validation.ValidateIntegerPositiveValue("Client ID: ");

                    existingClient = clientList.Find(m => m.id == client.id);

                    if (existingClient == null)
                        Console.WriteLine("\nClient wasn't found in database and cannot be deleted.");
                }
                while (existingClient == null);

                if (clientManager.Delete(client))
                    Console.WriteLine("\nClient was deleted from database succesfully.");
                else
                    Console.WriteLine("\nClient couldn't be deleted from database.");

                Console.Write("Delete another client? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());

            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void UpdateClient()
        {
            do
            {
                do
                {
                    List<Client> clientList = clientManager.Select();

                    do
                    {
                        client.id = validation.ValidateIntegerPositiveValue("Client ID: ");

                        existingClient = clientList.Find(m => m.id == client.id);

                        if (existingClient == null)
                            Console.WriteLine("\nClient wasn't found in database and cannot be modified.");
                    }
                    while (existingClient == null);

                    Console.Write("\nClient Name: ");
                    client.name = Console.ReadLine();
                    Console.Write("\nClient RFC: ");
                    client.rfc = Console.ReadLine();
                    Console.Write("\nClient Direction: ");
                    client.direction = Console.ReadLine();

                    errors = validateClient.ValidateObject(client);

                    if (errors.Any())
                    {
                        foreach (var error in errors)
                        {
                            Console.WriteLine(error.ErrorMessage);
                        }
                    }

                } while (errors.Count > 0);

                if (clientManager.Update(client))
                    Console.WriteLine("\nClient was updated in the database succesfully.");
                else
                    Console.WriteLine("\nClient couldn't be updated in the database.");

                Console.Write("Update another client? (y / n): ");
                loopOption = Char.Parse(Console.ReadLine());
            }
            while (loopOption == 'y');

            additions.ClearScreenAfterKey();
        }

        public void ListAllClients()
        {
            List<Client> clientList = clientManager.Select();

            string id = "ID", name = "Name", rfc = "RFC", direction = "Direction";

            Console.WriteLine($"{id.PadRight(5)}{name.PadRight(40)}{rfc.PadRight(15)}{direction.PadRight(40)}\n");

            foreach (var clients in clientList)
            {
                Console.WriteLine(clients.id.ToString().PadRight(5) +
                    clients.name.PadRight(40) +
                    clients.rfc.PadRight(15) +
                    clients.direction.PadRight(40));
            }

            additions.ClearScreenAfterKey();
        }
    }
}