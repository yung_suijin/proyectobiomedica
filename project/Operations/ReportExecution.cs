﻿using library;
using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace project.Operations
{
    internal class ReportExecution
    {
        private Client client = new Client();
        private MedicalEquipment product = new MedicalEquipment();
        private Client existingClient;
        private MedicalEquipment existingProduct;
        private readonly ClientManager clientManager = new ClientManager("dbConnection");
        private readonly MedicalEquipmentManager medicalEquipmentManager = new MedicalEquipmentManager("dbConnection");
        private readonly BillManager billManager = new BillManager("dbConnection");
        public Validations validation = new Validations();
        public Additions additions = new Additions();
        public string id = "ID", name = "Name", sellDate = "Date", quantity = "Quantity", unitaryPrice = "Unitary Price",
            total = "Total", taxes = "Taxes", commission = "Commission", productName = "Product", subtotal = "Subtotal";

        public void ReportByProduct()
        {
            List<MedicalEquipment> medicalEquipmentList = medicalEquipmentManager.Select();
            List<Bill> billsList = billManager.Select();

            do
            {
                product.id = validation.ValidateIntegerPositiveValue("Product ID: ");

                existingProduct = medicalEquipmentList.Find(m => m.id == product.id);

                if (existingProduct == null)
                    Console.WriteLine("\nProduct wasn't found in database.");
            }
            while (existingProduct == null);

            var productIdList = billsList.Where(b => b.productId == product.id);

            Console.WriteLine($"\n{name.PadRight(40)}{sellDate.PadRight(15)}{quantity.PadRight(13)}{unitaryPrice.PadRight(15)}" +
                $"{subtotal.PadRight(12)}{taxes.PadRight(10)}{total.PadRight(10)}\n");

            foreach (var info in productIdList)
            {
                Console.WriteLine(info.productName.PadRight(40) +
                    info.date.ToString(@"dd\/MM\/yyyy").PadRight(15) +
                    info.quantity.ToString().PadRight(13) +
                    info.unitaryPrice.ToString().PadRight(15) +
                    info.subtotal.ToString().PadRight(12) +
                    info.taxes.ToString().PadRight(10) +
                    info.totalPrice.ToString().PadRight(10));
            }

            additions.ClearScreenAfterKey();
        }

        public void ReportByClient()
        {
            List<Client> clientList = clientManager.Select();
            List<Bill> billsList = billManager.Select();

            do
            {
                client.id = validation.ValidateIntegerPositiveValue("Client ID: ");

                existingClient = clientList.Find(m => m.id == client.id);

                if (existingClient == null)
                    Console.WriteLine("\nClient wasn't found in database.");
            }
            while (existingClient == null);

            var clientIdList = billsList.Where(b => b.clientId == client.id);

            Console.WriteLine($"\n{name.PadRight(40)}{sellDate.PadRight(15)}{name.PadRight(40)}{quantity.PadRight(13)}{unitaryPrice.PadRight(15)}" +
                $"{subtotal.PadRight(12)}{taxes.PadRight(10)}{total.PadRight(10)}\n");

            foreach (var info in clientIdList)
            {
                Console.WriteLine(info.clientName.PadRight(40) +
                    info.date.ToString(@"dd\/MM\/yyyy").PadRight(15) +
                    info.productName.PadRight(40) +
                    info.quantity.ToString().PadRight(13) +
                    info.unitaryPrice.ToString().PadRight(15) +
                    info.subtotal.ToString().PadRight(12) +
                    info.taxes.ToString().PadRight(10) +
                    info.totalPrice.ToString().PadRight(10));
            }

            additions.ClearScreenAfterKey();
        }
    }
}
