﻿using library;
using project.Operations;
using System;

namespace project
{
    internal class ClientsMenu
    {
        public AdministratorMenu administratorMenu = new AdministratorMenu();
        public ClientExecution execution = new ClientExecution();
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;

            do
            {
                Console.WriteLine("1. New client.");
                Console.WriteLine("2. Delete client.");
                Console.WriteLine("3. Modify client.");
                Console.WriteLine("4. Clients info.");
                Console.WriteLine("5. Administrator menu.");
                option = validations.ValidateIntegerPositiveValue("\nOption: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        execution.NewClient();
                        administratorMenu.Menu();
                    break;

                    case 2:
                        Console.Clear();
                        execution.DeleteClient();
                        administratorMenu.Menu();
                    break;

                    case 3:
                        Console.Clear();
                        execution.UpdateClient();
                        administratorMenu.Menu();
                    break;

                    case 4:
                        Console.Clear();
                        execution.ListAllClients();
                        administratorMenu.Menu();
                    break;

                    case 5:
                        Console.Clear();
                        administratorMenu.Menu();
                    break;
                }
            }
            while (option < 1 || option > 5);
        }
    }
}