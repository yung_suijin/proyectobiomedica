﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using library.DataAccess;
using project.POCO;

namespace project.Managers
{
    internal class MedicalEquipmentManager : iDatabase<MedicalEquipment>
    {
        private Connection Connection { get; set; }

        public MedicalEquipmentManager(string key)
        {
            Connection = new Connection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
        }

        public bool Insert(MedicalEquipment element)
        {
            var insertCommand = "INSERT INTO [dbo].[inventory] " + 
                "(productId, productName, productQuantity, productTrademark, productModel, productLot, productBuyingPrice, productSellingPrice)" +
                " VALUES (@idParameter, @nameParameter, @quantityParameter, @trademarkParameter, @modelParameter, @lotParameter, @priceBuyParameter, @priceSellParameter)";
            
            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar, 100) { Value = element.name },
                new SqlParameter("@quantityParameter", SqlDbType.Int) { Value = 0 },
                new SqlParameter("@trademarkParameter", SqlDbType.VarChar, 50) { Value = element.trademark },
                new SqlParameter("@modelParameter", SqlDbType.VarChar, 50) { Value = element.model },
                new SqlParameter("@lotParameter", SqlDbType.VarChar, 15) { Value = element.lot },
                new SqlParameter("@priceBuyParameter", SqlDbType.Float) { Value = element.buyingPrice },
                new SqlParameter("@priceSellParameter", SqlDbType.Float) { Value = element.sellingPrice }
            };

            return Connection.ExecuteCommand(insertCommand, parameters);
        }

        public bool Delete(MedicalEquipment element)
        {
            var deleteCommand = "DELETE FROM [dbo].[inventory] WHERE productId = @idParameter";

            SqlParameter[] parameter =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id }
            };

            return Connection.ExecuteCommand(deleteCommand, parameter);
        }

        public bool Update(MedicalEquipment element)
        {
            var updateCommand = "UPDATE [dbo].[inventory] SET productName = @nameParameter, " +
                "productTrademark = @trademarkParameter, productModel = @modelParameter, productLot = @lotParameter, productBuyingPrice = @priceBuyParameter, " +
                "productSellingPrice = @priceSellParameter WHERE productId = @idParameter";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar, 100) { Value = element.name },
                new SqlParameter("@trademarkParameter", SqlDbType.VarChar, 50) { Value = element.trademark },
                new SqlParameter("@modelParameter", SqlDbType.VarChar, 50) { Value = element.model },
                new SqlParameter("@lotParameter", SqlDbType.VarChar, 15) { Value = element.lot },
                new SqlParameter("@priceBuyParameter", SqlDbType.Float) { Value = element.buyingPrice },
                new SqlParameter("@priceSellParameter", SqlDbType.Float) { Value = element.sellingPrice }
            };

            return Connection.ExecuteCommand(updateCommand, parameters);
        }

        public List<MedicalEquipment> Select()
        {
            var medicalEquipmentList = new List<MedicalEquipment>();
            var selectCommand = "SELECT [productId],[productName],[productQuantity],[productTrademark],[productModel]," +
                "[productLot],[productBuyingPrice],[productSellingPrice]" + " FROM[dbo].[inventory]";

            var data = Connection.Lecture(selectCommand);

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var medicalEquipment = new MedicalEquipment();

                medicalEquipment.id = Convert.ToInt32(row[0]);
                medicalEquipment.name = row[1].ToString();
                medicalEquipment.quantity = Convert.ToInt32(row[2]);
                medicalEquipment.trademark = row[3].ToString();
                medicalEquipment.model = row[4].ToString();
                medicalEquipment.lot = row[5].ToString();
                medicalEquipment.buyingPrice = Convert.ToDouble(row[6]);
                medicalEquipment.sellingPrice = Convert.ToDouble(row[7]);

                medicalEquipmentList.Add(medicalEquipment);
            }

            return medicalEquipmentList;
        }

        public bool UpdateExistences(MedicalEquipment element)
        {
            var updateCommand = "UPDATE [dbo].[inventory] SET productName = @nameParameter, productQuantity = @quantityParameter, " +
                "productTrademark = @trademarkParameter, productModel = @modelParameter, productLot = @lotParameter, productBuyingPrice = @priceBuyParameter, " +
                "productSellingPrice = @priceSellParameter WHERE productId = @idParameter";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar, 100) { Value = element.name },
                new SqlParameter("@quantityParameter", SqlDbType.Int) { Value = element.quantity },
                new SqlParameter("@trademarkParameter", SqlDbType.VarChar, 50) { Value = element.trademark },
                new SqlParameter("@modelParameter", SqlDbType.VarChar, 50) { Value = element.model },
                new SqlParameter("@lotParameter", SqlDbType.VarChar, 15) { Value = element.lot },
                new SqlParameter("@priceBuyParameter", SqlDbType.Float) { Value = element.buyingPrice },
                new SqlParameter("@priceSellParameter", SqlDbType.Float) { Value = element.sellingPrice }
            };

            return Connection.ExecuteCommand(updateCommand, parameters);
        }
    }
}