﻿using library.DataAccess;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace project.Managers
{
    internal class ProviderManager : iDatabase<Provider>
    {
        private Connection Connection { get; set; }

        public ProviderManager(string key)
        {
            Connection = new Connection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
        }

        public bool Insert(Provider element)
        {
            var insertCommand = "INSERT INTO [dbo].[providers] " +
                "(providerId, providerName, providerRfc, providerDirection)" +
                " VALUES (@idParameter, @nameParameter, @rfcParameter, @directionParameter)";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar, 100) { Value = element.name },
                new SqlParameter("@rfcParameter", SqlDbType.VarChar, 15) { Value = element.rfc },
                new SqlParameter("@directionParameter", SqlDbType.VarChar, 200) { Value = element.direction }
            };

            return Connection.ExecuteCommand(insertCommand, parameters);
        }

        public bool Delete(Provider element)
        {
            var deleteCommand = "DELETE FROM [dbo].[providers] WHERE providerId = @idParameter";

            SqlParameter[] parameter =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id }
            };

            return Connection.ExecuteCommand(deleteCommand, parameter);
        }

        public bool Update(Provider element)
        {
            var updateCommand = "UPDATE [dbo].[providers] SET providerName = @nameParameter, providerRfc = @rfcParameter, " +
                "providerDirection = @directionParameter WHERE providerId = @idParameter";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar, 100) { Value = element.name },
                new SqlParameter("@rfcParameter", SqlDbType.VarChar, 15) { Value = element.rfc },
                new SqlParameter("@directionParameter", SqlDbType.VarChar, 200) { Value = element.direction }
            };

            return Connection.ExecuteCommand(updateCommand, parameters);
        }

        public List<Provider> Select()
        {
            var providerList = new List<Provider>();
            var selectCommand = "SELECT [providerId],[providerName],[providerRfc],[providerDirection] FROM [dbo].[providers]";

            var data = Connection.Lecture(selectCommand);

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var provider = new Provider();

                provider.id = Convert.ToInt32(row[0]);
                provider.name = row[1].ToString();
                provider.rfc = row[2].ToString();
                provider.direction = row[3].ToString();

                providerList.Add(provider);
            }

            return providerList;
        }
    }
}
