﻿using library.DataAccess;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace project.Managers
{
    internal class UserManager : iDatabase<User>
    {
        private Connection Connection { get; set; }

        public UserManager(string key)
        {
            Connection = new Connection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
        }

        public bool Session(User user)
        {
            var storedProcedure = "ObtainUserLevel";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = user.id },
                new SqlParameter("@passwordParameter", SqlDbType.VarChar, 30) { Value = user.password }
            };

            var response = Connection.ExecuteStoredProcedure(storedProcedure, parameters);

            if (Convert.ToInt32(response) > 0)
            {
                user.level = (int)response;
                return true;
            }

            return false;
        }

        public bool Insert(User element)
        {
            var insertCommand = "INSERT INTO [dbo].[users] " +
                "(userId, userPassword, userLevel, userCommissions)" +
                " VALUES (@idParameter,@passwordParameter, @levelParameter)";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@passwordParameter", SqlDbType.VarChar, 30) { Value = element.password },
                new SqlParameter("@levelParameter", SqlDbType.Int) { Value = element.level }
            };

            return Connection.ExecuteCommand(insertCommand, parameters);
        }

        public bool Delete(User element)
        {
            var deleteCommand = "DELETE FROM [dbo].[users] WHERE userId = @idParameter";

            SqlParameter[] parameter =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id }
            };

            return Connection.ExecuteCommand(deleteCommand, parameter);
        }

        public bool Update(User element)
        {
            var updateCommand = "UPDATE [dbo].[users] SET userPassword = @passwordParameter, " +
                "userLevel = @levelParameter WHERE userId = @idParameter";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@passwordParameter", SqlDbType.VarChar, 30) { Value = element.password },
                new SqlParameter("@levelParameter", SqlDbType.Int) { Value = element.level }
            };

            return Connection.ExecuteCommand(updateCommand, parameters);
        }

        public List<User> Select()
        {
            var userList = new List<User>();
            var selectCommand = "SELECT [userId],[userPassword],[userLevel] FROM [dbo].[users]";

            var data = Connection.Lecture(selectCommand);

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var user = new User();

                user.id = Convert.ToInt32(row[0]);
                user.password = row[1].ToString();
                user.level = Convert.ToInt32(row[2]);

                userList.Add(user);
            }

            return userList;
        }
    }
}