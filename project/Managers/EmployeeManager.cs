﻿using library.DataAccess;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace project.Managers
{
    internal class EmployeeManager : iDatabase<Employee>
    {
        private Connection Connection { get; set; }

        public EmployeeManager(string key)
        {
            Connection = new Connection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
        }

        public bool Insert(Employee element)
        {
            var insertCommand = "INSERT INTO [dbo].[employees] " +
                "(employeeId, employeeName, employeeRfc, employeeRole, employeeSalary, employeeCommissions)" +
                " VALUES (@idParameter, @nameParameter, @rfcParameter, @roleParameter, @salaryParameter, @commissionParameter)";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar) { Value = element.name },
                new SqlParameter("@rfcParameter", SqlDbType.VarChar) { Value = element.rfc },
                new SqlParameter("@roleParameter", SqlDbType.VarChar) { Value = element.role },
                new SqlParameter("@salaryParameter", SqlDbType.Float) { Value = element.salary },
                new SqlParameter("@commissionParameter", SqlDbType.Float) { Value = 0 }
            };

            return Connection.ExecuteCommand(insertCommand, parameters);
        }

        public bool Delete(Employee element)
        {
            var deleteCommand = "DELETE FROM [dbo].[employees] WHERE employeeId = @idParameter";

            SqlParameter[] parameter =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id }
            };

            return Connection.ExecuteCommand(deleteCommand, parameter);
        }

        public bool Update(Employee element)
        {
            var updateCommand = "UPDATE [dbo].[employees] SET employeeName = @nameParameter, employeeRfc = @rfcParameter, " +
                "employeeRole = @roleParameter, employeeSalary = @salaryParameter WHERE employeeId = @idParameter";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar) { Value = element.name },
                new SqlParameter("@rfcParameter", SqlDbType.VarChar) { Value = element.rfc },
                new SqlParameter("@roleParameter", SqlDbType.VarChar) { Value = element.role },
                new SqlParameter("@salaryParameter", SqlDbType.Float) { Value = element.salary }
            };

            return Connection.ExecuteCommand(updateCommand, parameters);
        }

        public bool UpdateCommissions(Employee element)
        {
            var updateCommand = "UPDATE [dbo].[employees] SET employeeName = @nameParameter, employeeRfc = @rfcParameter, " +
                "employeeRole = @roleParameter, employeeSalary = @salaryParameter, employeeCommissions = @commissionParameter WHERE employeeId = @idParameter";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar) { Value = element.name },
                new SqlParameter("@rfcParameter", SqlDbType.VarChar) { Value = element.rfc },
                new SqlParameter("@roleParameter", SqlDbType.VarChar) { Value = element.role },
                new SqlParameter("@salaryParameter", SqlDbType.Float) { Value = element.salary },
                new SqlParameter("@commissionParameter", SqlDbType.Float) { Value = element.commission }
            };

            return Connection.ExecuteCommand(updateCommand, parameters);
        }

        public List<Employee> Select()
        {
            var employeeList = new List<Employee>();
            var selectCommand = "SELECT [employeeId],[employeeName],[employeeRfc],[employeeRole],[employeeSalary],[employeeCommissions] FROM[dbo].[employees]";

            var data = Connection.Lecture(selectCommand);

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var employee = new Employee();

                employee.id = Convert.ToInt32(row[0]);
                employee.name = row[1].ToString();
                employee.rfc = row[2].ToString();
                employee.role = row[3].ToString();
                employee.salary = Convert.ToDouble(row[4]);
                employee.commission = Convert.ToDouble(row[5]);

                employeeList.Add(employee);
            }

            return employeeList;
        }
    }
}
