﻿using library.DataAccess;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace project.Managers
{
    internal class ClientManager : iDatabase<Client>
    {
        private Connection Connection { get; set; }

        public ClientManager(string key)
        {
            Connection = new Connection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
        }

        public bool Insert(Client element)
        {
            var insertCommand = "INSERT INTO [dbo].[clients] " +
                "(clientId, clientName, clientRfc, clientDirection)" +
                " VALUES (@idParameter, @nameParameter, @rfcParameter, @directionParameter)";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar, 100) { Value = element.name },
                new SqlParameter("@rfcParameter", SqlDbType.VarChar, 15) { Value = element.rfc },
                new SqlParameter("@directionParameter", SqlDbType.VarChar, 200) { Value = element.direction }
            };

            return Connection.ExecuteCommand(insertCommand, parameters);
        }

        public bool Delete(Client element)
        {
            var deleteCommand = "DELETE FROM [dbo].[clients] WHERE clientId = @idParameter";

            SqlParameter[] parameter =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id }
            };

            return Connection.ExecuteCommand(deleteCommand, parameter);
        }

        public bool Update(Client element)
        {
            var updateCommand = "UPDATE [dbo].[clients] SET clientName = @nameParameter, clientRfc = @rfcParameter, " +
                "clientDirection = @directionParameter WHERE clientId = @idParameter";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.id },
                new SqlParameter("@nameParameter", SqlDbType.VarChar, 100) { Value = element.name },
                new SqlParameter("@rfcParameter", SqlDbType.VarChar, 15) { Value = element.rfc },
                new SqlParameter("@directionParameter", SqlDbType.VarChar, 200) { Value = element.direction }
            };

            return Connection.ExecuteCommand(updateCommand, parameters);
        }

        public List<Client> Select()
        {
            var clientList = new List<Client>();
            var selectCommand = "SELECT [clientId],[clientName],[clientRfc],[clientDirection] FROM [dbo].[clients]";

            var data = Connection.Lecture(selectCommand);

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var client = new Client();

                client.id = Convert.ToInt32(row[0]);
                client.name = row[1].ToString();
                client.rfc = row[2].ToString();
                client.direction = row[3].ToString();

                clientList.Add(client);
            }

            return clientList;
        }
    }
}