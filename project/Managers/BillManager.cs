﻿using library.DataAccess;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace project.Managers
{
    internal class BillManager
    {
        private Connection Connection { get; set; }

        public BillManager(string key)
        {
            Connection = new Connection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
        }

        public bool Insert(Bill element)
        {
            var insertCommand = "INSERT INTO [dbo].[bills] (billInvoice, billDate, clientId, clientName, clientRfc, productId, " +
                "productName, productQuantity, productTrademark, productModel, productSellingPrice, subtotal, taxes, totalPrice)" +
                " VALUES (@invoiceParameter, @dateParameter, @clientIdParameter, @clientNameParameter, @clientRfcParameter, @productIdParameter, " +
                "@productNameParameter, @quantityParameter, @productTrademarkParameter,@productModelParameter, @productSellingPriceParameter, " +
                "@subtotalParameter, @taxesParameter, @totalPriceParameter)";

            SqlParameter[] parameters =
            {
                new SqlParameter("@invoiceParameter", SqlDbType.Int) { Value = element.invoice },
                new SqlParameter("@dateParameter", SqlDbType.SmallDateTime) { Value = element.date },
                new SqlParameter("@clientIdParameter", SqlDbType.Int) { Value = element.clientId },
                new SqlParameter("@clientNameParameter", SqlDbType.VarChar, 100) { Value = element.clientName },
                new SqlParameter("@clientRfcParameter", SqlDbType.VarChar, 13) { Value = element.clientRfc },
                new SqlParameter("@productIdParameter", SqlDbType.Int) { Value = element.productId },
                new SqlParameter("@productNameParameter", SqlDbType.VarChar, 100) { Value = element.productName },
                new SqlParameter("@quantityParameter", SqlDbType.Int) { Value = element.quantity },
                new SqlParameter("@productTrademarkParameter", SqlDbType.VarChar, 50) { Value = element.productTrademark },
                new SqlParameter("@productModelParameter", SqlDbType.VarChar, 50) { Value = element.productModel },
                new SqlParameter("@productSellingPriceParameter", SqlDbType.Float) { Value = element.unitaryPrice },
                new SqlParameter("@subtotalParameter", SqlDbType.Float) { Value = element.subtotal },
                new SqlParameter("@taxesParameter", SqlDbType.Float) { Value = element.taxes },
                new SqlParameter("@totalPriceParameter", SqlDbType.Float) { Value = element.totalPrice }
            };

            return Connection.ExecuteCommand(insertCommand, parameters);
        }

        public List<Bill> Select()
        {
            var billList = new List<Bill>();
            var selectCommand = "SELECT [billInvoice],[billDate],[clientId],[clientName],[clientRfc],[productId],[productName],[productQuantity]," +
                "[productTrademark],[productModel],[productSellingPrice],[subtotal],[taxes],[totalPrice] FROM[dbo].[bills]";

            var data = Connection.Lecture(selectCommand);

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var bill = new Bill();

                bill.invoice = Convert.ToInt32(row[0]);
                bill.date = Convert.ToDateTime(row[1]);
                bill.clientId = Convert.ToInt32(row[2]);
                bill.clientName = row[3].ToString();
                bill.clientRfc = row[4].ToString();
                bill.productId = Convert.ToInt32(row[5]);
                bill.productName = row[6].ToString();
                bill.quantity = Convert.ToInt32(row[7]);
                bill.productTrademark = row[8].ToString();
                bill.productModel = row[9].ToString();
                bill.unitaryPrice = Convert.ToDouble(row[10]);
                bill.subtotal = Convert.ToDouble(row[11]);
                bill.taxes = Convert.ToDouble(row[12]);
                bill.totalPrice = Convert.ToDouble(row[13]);

                billList.Add(bill);
            }

            return billList;
        }
    }
}
