﻿using library.DataAccess;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace project.Managers
{
    internal class PurchaseManager
    {
        private Connection Connection { get; set; }

        public PurchaseManager(string key)
        {
            Connection = new Connection(ConfigurationManager.ConnectionStrings[key].ConnectionString);
        }

        public bool Insert(Purchase element)
        {
            var insertCommand = "INSERT INTO [dbo].[purchases] " +
                "(purchaseId, purchaseDate, providerId, providerName, providerRfc, productId, productName, productModel, productQuantity, productBuyingPrice, " +
                "subtotal, taxes, totalPrice)" +
                " VALUES (@idParameter, @dateParameter, @providerIdParameter, @providerNameParameter, @providerRfcParameter, @productIdParameter, @productNameParameter, @modelParameter, @quantityParameter, " +
                " @buyingPriceParameter, @subtotalParameter, @taxesParameter, @totalParameter)";

            SqlParameter[] parameters =
            {
                new SqlParameter("@idParameter", SqlDbType.Int) { Value = element.purchaseId },
                new SqlParameter("@dateParameter", SqlDbType.SmallDateTime) { Value = element.date },
                new SqlParameter("@providerIdParameter", SqlDbType.Int) { Value = element.providerId },
                new SqlParameter("@providerNameParameter", SqlDbType.VarChar, 100) { Value = element.providerName },
                new SqlParameter("@providerRfcParameter", SqlDbType.VarChar, 15) { Value = element.providerRfc },
                new SqlParameter("@ProductIdParameter", SqlDbType.Int) { Value = element.productId },
                new SqlParameter("@productNameParameter", SqlDbType.VarChar, 100) { Value = element.productName },
                new SqlParameter("@modelParameter", SqlDbType.VarChar, 50) { Value = element.model },
                new SqlParameter("@quantityParameter", SqlDbType.Int) { Value = element.quantity },
                new SqlParameter("@buyingPriceParameter", SqlDbType.Float) { Value = element.buyingPrice },
                new SqlParameter("@subtotalParameter", SqlDbType.Float) { Value = element.subtotal },
                new SqlParameter("@taxesParameter", SqlDbType.Float) { Value = element.taxes },
                new SqlParameter("@totalParameter", SqlDbType.Float) { Value = element.totalPrice },
            };

            return Connection.ExecuteCommand(insertCommand, parameters);
        }

        public List<Purchase> Select()
        {
            var purchasesList = new List<Purchase>();
            var selectCommand = "SELECT [purchaseId],[purchaseDate],[providerId],[providerName],[providerRfc],[productId],[productName],[productModel],[productQuantity], " +
                "[productBuyingPrice],[subtotal],[taxes],[totalPrice] FROM [dbo].[purchases]";

            var data = Connection.Lecture(selectCommand);

            foreach (DataRow row in data.Tables[0].Rows)
            {
                var purchase = new Purchase();

                purchase.purchaseId = Convert.ToInt32(row[0]);
                purchase.date = Convert.ToDateTime(row[1]);
                purchase.providerId = Convert.ToInt32(row[2]);
                purchase.providerName = row[3].ToString();
                purchase.providerRfc = row[4].ToString();
                purchase.productId = Convert.ToInt32(row[5]);
                purchase.productName = row[6].ToString();
                purchase.model = row[7].ToString();
                purchase.quantity = Convert.ToInt32(row[8]);
                purchase.buyingPrice = Convert.ToDouble(row[9]);
                purchase.subtotal = Convert.ToDouble(row[10]);
                purchase.taxes = Convert.ToDouble(row[11]);
                purchase.totalPrice = Convert.ToDouble(row[12]);

                purchasesList.Add(purchase);
            }

            return purchasesList;
        }
    }
}
