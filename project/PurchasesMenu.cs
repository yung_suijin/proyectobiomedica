﻿using library;
using project.Operations;
using System;
using System.Collections.Generic;
using System.Text;

namespace project
{
    internal class PurchasesMenu
    {
        public AdministratorMenu administratorMenu = new AdministratorMenu();
        public PurchaseExecution purchaseExecution = new PurchaseExecution();
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;

            do
            {
                Console.WriteLine("1. New purchase.");
                Console.WriteLine("2. Purchase report by product.");
                Console.WriteLine("3. Purchase report by provider.");
                Console.WriteLine("4. Administrator menu.");
                option = validations.ValidateIntegerPositiveValue("Option: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        purchaseExecution.BuyProduct();
                        administratorMenu.Menu();
                        break;
                    case 2:
                        Console.Clear();
                        purchaseExecution.PurchaseReportByProduct();
                        administratorMenu.Menu();
                        break;
                    case 3:
                        Console.Clear();
                        purchaseExecution.PurchaseReportByProvider();
                        administratorMenu.Menu();
                        break;
                    case 4:
                        Console.Clear();
                        administratorMenu.Menu();
                        break;
                }
            }
            while (option < 1 || option > 3);
        }
    }
}