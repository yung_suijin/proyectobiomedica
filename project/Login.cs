﻿using library;
using project.Managers;
using project.POCO;
using System;

namespace project
{
    public class Login
    {
        public void login()
        {
            var administratorMenu = new AdministratorMenu();
            var billing = new Billing();
            var warehouse = new Warehouse();
            var userManager = new UserManager("dbConnection");
            var validation = new Validations();
            var addition = new Additions();
            var user = new User();
            var usersList = userManager.Select();
            var existingUser = new User();

            Console.WriteLine("-------------------------------");
            Console.WriteLine("|            Login            |");
            Console.WriteLine("-------------------------------");

            do
            {
                user.id = validation.ValidateIntegerPositiveValue("\nUser ID: ");

                do
                {
                    Console.Write("\nPassword: ");
                    user.password = addition.HideKeyboardPassword();
                } 
                while (user.password.Length == 0);

                if (userManager.Session(user))
                {
                    existingUser = usersList.Find(m => m.id == user.id);

                    if (existingUser.level == 1)
                    {
                        Console.Clear();
                        administratorMenu.Menu();
                    }
                    else if (existingUser.level == 2)
                    {
                        Console.Clear();
                        billing.GenerateBill();
                    }
                    else if (existingUser.level == 3)
                    {
                        Console.Clear();
                        warehouse.CheckWarehouse();
                    }
                }
                else
                    Console.WriteLine("\nInvalid credentials.");
            }
            while (!userManager.Session(user));
        }
    }
}
