﻿using project.Managers;
using project.POCO;
using System;
using System.Collections.Generic;
using System.Text;

namespace project
{
    internal class Warehouse
    {
        private readonly MedicalEquipmentManager medicalEquipmentManager = new MedicalEquipmentManager("dbConnection");

        public void CheckWarehouse()
        {
            Console.WriteLine("---------------------------------");
            Console.WriteLine("|            Warehouse          |");
            Console.WriteLine("---------------------------------");

            List<MedicalEquipment> medicalEquipmentList = medicalEquipmentManager.Select();

            string id = "ID", name = "Name", quantity = "Quantity", trademark = "Trademark",
                        model = "Model", lot = "Lot";

            Console.WriteLine($"\n{id.PadRight(5)}{name.PadRight(40)}{quantity.PadRight(10)}{trademark.PadRight(15)}" +
                $"{model.PadRight(15)}{lot.PadRight(15)}\n");

            foreach (var product in medicalEquipmentList)
            {
                Console.WriteLine(product.id.ToString().PadRight(5) +
                    product.name.PadRight(40) +
                    product.quantity.ToString().PadRight(10) +
                    product.trademark.PadRight(15) +
                    product.model.PadRight(15) +
                    product.lot.PadRight(15));
            }
        }
    }
}
