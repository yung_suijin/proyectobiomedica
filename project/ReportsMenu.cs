﻿using library;
using project.Operations;
using System;

namespace project
{
    internal class ReportsMenu
    {
        public AdministratorMenu administratorMenu = new AdministratorMenu();
        public ReportExecution reportExecution = new ReportExecution();
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;

            do
            {
                Console.WriteLine("1. Selling report by product.");
                Console.WriteLine("2. Selling report by client.");
                Console.WriteLine("3. Administrator menu.");
                option = validations.ValidateIntegerPositiveValue("Option: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        reportExecution.ReportByProduct();
                        administratorMenu.Menu();
                        break;
                    case 2:
                        Console.Clear();
                        reportExecution.ReportByClient();
                        administratorMenu.Menu();
                        break;
                    case 3:
                        Console.Clear();
                        administratorMenu.Menu();
                        break;
                }
            } 
            while (option < 1 || option > 3);
        }
    }
}