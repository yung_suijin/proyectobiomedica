﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace project.POCO
{
    internal class Bill
    {
        [Required]
        public int invoice { get; set; }

        [Required]
        public DateTime date { get; set; }

        [Required]
        public int clientId { get; set; }

        [Required, StringLength(100)]
        public string clientName { get; set; }

        [Required, StringLength(13)]
        public string clientRfc { get; set; }

        [Required]
        public int productId { get; set; }

        [Required, StringLength(100)]
        public string productName { get; set; }

        [Required]
        public int quantity { get; set; }

        [Required, StringLength(50)]
        public string productTrademark { get; set; }

        [Required, StringLength(50)]
        public string productModel { get; set; }

        [Required]
        public double unitaryPrice { get; set; }

        [Required]
        public double subtotal { get; set; }

        [Required]
        public double taxes { get; set; }

        [Required]
        public double totalPrice { get; set; }
    }
}
