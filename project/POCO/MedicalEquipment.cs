﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace project.POCO
{
    internal class MedicalEquipment
    {
        public int id { get; set; }

        [Required, StringLength(100, ErrorMessage = "\nMaximum length reached.")]
        public string name { get; set; }

        [Required]
        public int quantity { get; set; }

        [Required, StringLength(50, ErrorMessage = "\nMaximum length reached.")]
        public string trademark { get; set; }

        [Required, StringLength(50, ErrorMessage = "\nMaximum length reached.")]
        public string model { get; set; }

        [Required, StringLength(15, ErrorMessage = "\nMaximum length reached.")]
        public string lot { get; set; }

        [Required]
        public double buyingPrice { get; set; }

        [Required]
        public double sellingPrice { get; set; }
    }
}