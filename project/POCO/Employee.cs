﻿using System.ComponentModel.DataAnnotations;

namespace project.POCO
{
    internal class Employee
    {
        public int id { get; set; }

        [Required, StringLength(100, ErrorMessage = "\nMaximum length reached.")]
        public string name { get; set; }

        [Required, StringLength(15, ErrorMessage = "\nMaximum length reached.")]
        public string rfc { get; set; }

        [Required, StringLength(30, ErrorMessage = "\nMaximum length reached.")]
        public string role { get; set; }

        [Required]
        public double salary { get; set; }

        [Required]
        public double commission { get; set; }
    }
}