﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace project.POCO
{
    internal class User
    {
        [Required]
        public int id { get; set; }

        [Required, StringLength(30, ErrorMessage = "\nMaximum length reached.")]
        public string password { get; set; }

        [Required]
        public int level { get; set; }
    }
}