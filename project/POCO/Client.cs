﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace project.POCO
{
    internal class Client
    {
        public int id { get; set; }

        [Required, StringLength(100, ErrorMessage = "\nMaximum length reached.")]
        public string name { get; set; }

        [Required, StringLength(15, ErrorMessage = "\nMaximum length reached.")]
        public string rfc { get; set; }

        [Required, StringLength(200, ErrorMessage = "\nMaximum length reached.")]
        public string direction { get; set; }
    }
}
