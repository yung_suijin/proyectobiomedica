﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace project.POCO
{
    internal class Purchase
    {
        [Required]
        public int purchaseId { get; set; }

        [Required]
        public DateTime date { get; set; }

        [Required]
        public int providerId { get; set; }

        [Required, StringLength(100)]
        public string providerName { get; set; }

        [Required, StringLength(15)]
        public string providerRfc { get; set; }

        [Required]
        public int productId { get; set; }

        [Required, StringLength(100)]
        public string productName { get; set; }

        [Required, StringLength(30)]

        public string model { get; set; }

        [Required]
        public int quantity { get; set; }

        [Required]
        public double buyingPrice { get; set; }

        [Required]
        public double subtotal { get; set; }

        [Required]
        public double taxes { get; set; }

        [Required]
        public double totalPrice { get; set; }
    }
}
