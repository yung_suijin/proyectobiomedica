﻿using library;
using project.Operations;
using System;

namespace project
{
    internal class ProvidersMenu
    {
        public AdministratorMenu administratorMenu = new AdministratorMenu();
        public ProviderExecution execution = new ProviderExecution();
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;

            do
            {
                Console.WriteLine("1. New provider.");
                Console.WriteLine("2. Delete provider.");
                Console.WriteLine("3. Modify provider.");
                Console.WriteLine("4. Providers info.");
                Console.WriteLine("5. Administrator menu.");
                option = validations.ValidateIntegerPositiveValue("\nOption: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        execution.NewProvider();
                        administratorMenu.Menu();
                        break;

                    case 2:
                        Console.Clear();
                        execution.DeleteProvider();
                        administratorMenu.Menu();
                        break;

                    case 3:
                        Console.Clear();
                        execution.UpdateProvider();
                        administratorMenu.Menu();
                        break;

                    case 4:
                        Console.Clear();
                        execution.ListAllProviders();
                        administratorMenu.Menu();
                        break;

                    case 5:
                        Console.Clear();
                        administratorMenu.Menu();
                        break;
                }
            }
            while (option < 1 || option > 5);
        }
    }
}
