﻿using System;
using library;
using project.Operations;

namespace project
{
    internal class AdministratorMenu
    {
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;
            var medicalEquipmentMenu = new MedicalEquipmentMenu();
            var clientsMenu = new ClientsMenu();
            var employeesMenu = new EmployeesMenu();
            var providersMenu = new ProvidersMenu();
            var usersMenu = new UsersMenu();
            var reports = new ReportsMenu();
            var purchases = new PurchasesMenu();

            Console.WriteLine("-------------------------------");
            Console.WriteLine("|           Welcome           |");
            Console.WriteLine("-------------------------------");

            do
            {
                Console.WriteLine("\n1. Medical Equipment.");
                Console.WriteLine("2. Clients.");
                Console.WriteLine("3. Employees.");
                Console.WriteLine("4. Providers.");
                Console.WriteLine("5. Users.");
                Console.WriteLine("6. Reports.");
                Console.WriteLine("7. Purchases.");
                Console.WriteLine("8. Exit.");
                option = validations.ValidateIntegerPositiveValue("\nOption: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        medicalEquipmentMenu.Menu();
                        break;
                    case 2:
                        Console.Clear();
                        clientsMenu.Menu();
                        break;
                    case 3:
                        Console.Clear();
                        employeesMenu.Menu();
                        break;
                    case 4:
                        Console.Clear();
                        providersMenu.Menu();
                        break;
                    case 5:
                        Console.Clear();
                        usersMenu.Menu();
                        break;
                    case 6:
                        Console.Clear();
                        reports.Menu();
                        break;
                    case 7:
                        Console.Clear();
                        purchases.Menu();
                        break;
                    case 8:
                        System.Environment.Exit(1);
                        break;
                }
            }
            while (option < 1 || option > 8);
        }
    }
}