﻿using library;
using project.Operations;
using System;

namespace project
{
    internal class UsersMenu
    {
        public AdministratorMenu administratorMenu = new AdministratorMenu();
        public UserExecution execution = new UserExecution();
        public Validations validations = new Validations();

        public void Menu()
        {
            int option;

            do
            {
                Console.WriteLine("1. New user.");
                Console.WriteLine("2. Delete user.");
                Console.WriteLine("3. Modify user.");
                Console.WriteLine("4. Users info.");
                Console.WriteLine("5. Administrator menu.");
                option = validations.ValidateIntegerPositiveValue("\nOption: ");

                switch (option)
                {
                    case 1:
                        Console.Clear();
                        execution.NewUser();
                        administratorMenu.Menu();
                        break;

                    case 2:
                        Console.Clear();
                        execution.DeleteUser();
                        administratorMenu.Menu();
                        break;

                    case 3:
                        Console.Clear();
                        execution.UpdateUser();
                        administratorMenu.Menu();
                        break;

                    case 4:
                        Console.Clear();
                        execution.ListAllUsers();
                        administratorMenu.Menu();
                        break;

                    case 5:
                        Console.Clear();
                        administratorMenu.Menu();
                        break;
                }
            }
            while (option < 1 || option > 5);
        }
    }
}